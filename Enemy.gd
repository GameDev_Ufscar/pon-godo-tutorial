extends KinematicBody2D

const MIN_DIST = 30
var SPEED = 2

func update_position(delta):
	var ball = get_parent().get_node_or_null("Ball")
	if ball:
		if abs(ball.position.y - self.position.y) > MIN_DIST:
			move_and_collide(Vector2(0,(ball.position.y - self.position.y)*delta*SPEED))
			
func _process(delta):
	update_position(delta)
