extends RigidBody2D

const START_SPEED = 10

func _ready():
	randomize()
	if randi() % 2 == 0:
		self.add_force(Vector2.ZERO,Vector2(-1,randf()-.5)*START_SPEED)
	else:
		self.add_force(Vector2.ZERO,Vector2(1,randf()-.5)*START_SPEED)
	self.bounce = 1


func _on_RigidBody2D_body_entered(body):
	if body is KinematicBody2D:
		self.add_force(Vector2.ZERO,self.applied_force*-2)
