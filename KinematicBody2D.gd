extends KinematicBody2D


var velocity = Vector2(0, 0)

func _physics_process(delta):
	velocity = Vector2(0,0)
	if Input.is_action_pressed("ui_up"):
		velocity = Vector2(0, -250)
	elif Input.is_action_pressed("ui_down"):
		velocity = Vector2(0, 250)
	var collision_info = move_and_collide(velocity*delta)
	collision_info
