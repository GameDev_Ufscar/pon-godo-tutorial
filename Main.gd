extends Node

var p1_score = 0
var p2_score = 0

func _ready():
	new_ball()
	p1_score = 0
	p2_score = 0

func _on_Area2D_body_entered(body):
	if body is RigidBody2D:
		body.free()
		new_ball()

func new_ball():
	var ball = load("res://Ball.tscn")
	self.add_child(ball.instance())


func _on_Player2Point_body_entered(body):
	if body is RigidBody2D:
		p2_score += 1
		$UI/P2.set_text(str(p2_score))


func _on_Player1Point_body_entered(body):
	if body is RigidBody2D:
		p1_score += 1
		$UI/P1.set_text(str(p1_score))
